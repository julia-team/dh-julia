#!/usr/bin/perl -w

use Parse::DebControl;
use Dpkg::Changelog;

my $parser = new Parse::DebControl;
my $data = $parser->parse_file ('./debian/control') -> [0];

for my $field ("Git", "Browser") {
    (my $tmp = $data -> {"Vcs-$field"}) =~ s/pkg-dev/\$name/;
    $data->{"Vcs-$field"} = $tmp;
}

open (FID, "< debian/compat");
$data->{Compat} = join ("", <FID>);
chomp $data->{Compat};
close FID;

open (FID, "< debian/changelog");
while (<FID>) {
    if (/\(([\d.]+)\)/) {
        $data->{Version} = $1;
        last;
    }
}

$data->{"Pkg-Branch"} = "metadata-v2";


$base = "make-julia-debpkg";
open (IN, "< $base.in");
open (OUT, "> $base");

while (<IN>) {
    for my $field ("Standards-Version", "Maintainer", "Pkg-Branch",
                   "Vcs-Git", "Vcs-Browser", "Compat", "Version") {
        ## Escape @ signs in email addresses
        (my $value = $data->{$field}) =~ s/@/\\@/g;
        s{#$field#}{$value};
    }
    print OUT;
}

close IN;
close OUT;


$base = "download-tarball";
open (IN, "< $base.in");
open (OUT, "> $base");

while (<IN>) {
    for my $field ("Pkg-Branch") {
        ## Escape @ signs in email addresses
        (my $value = $data->{$field}) =~ s/@/\\@/g;
        s{#$field#}{$value};
    }
    print OUT;
}

close IN;
close OUT;
