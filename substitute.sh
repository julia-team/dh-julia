#!/bin/bash

# Get a field from debian/control
get_control_field ()
{
    : ${1?No field given}
    grep -e "^$1:" debian/control | sed "s/^$1:\\s*//"
}

# In $1, replace #$2# by $3 
substitute_field ()
{
    filename=${1?No filename given}
    field=${2?No field name given}
    value="${3?No field value given}"

    # Escape slashes and dollars in $value
    value=${value//\//\\\/}
    value=${value//\$/\\\$}

    sed -i "s/#${field}#/$value/g" $filename
}

# Compute values to be substituted

declare -A val

for f in Vcs-Git Vcs-Browser Standards-Version
do
    val[$f]=$(get_control_field $f)
done

val[Vcs-Git]=${val[Vcs-Git]/pkg-dev/\$name}
val[Vcs-Browser]=${val[Vcs-Browser]/pkg-dev/\$name}

val[Compat]=$(cat debian/compat)
val[Version]=$(dpkg-parsechangelog --show-field Version)
val[Pkg-Branch]="metadata-v2"

# Create make-julia-debpkg

base=make-julia-debpkg
cp $base.in $base

for f in Vcs-Git Vcs-Browser Standards-Version Compat Version Pkg-Branch
do
    substitute_field $base $f "${val[$f]}"
done

# Create download-tarball

base=download-tarball
cp $base.in $base
substitute_field $base Pkg-Branch "${val[Pkg-Branch]}"
