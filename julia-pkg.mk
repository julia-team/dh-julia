include /usr/share/cdbs/1/rules/debhelper.mk

ifndef JULIA_PACKAGE_NAME
$(error Please define variable JULIA_PACKAGE_NAME)
endif

package = $(shell awk '/^Package:/ {print $$2; exit 0}' debian/control)
debpkg = debian/$(package)
instpath = /usr/share/julia/site/$(JULIA_PACKAGE_NAME)
docpath = /usr/share/doc/$(package)

build/$(package)::
	if test -f deps/build.jl; then \
		JULIA_PKGDIR=/nonexistent julia -f --no-history deps/build.jl; \
	fi

install/$(package)::
	mkdir -p $(debpkg)/$(instpath)
	for d in src deps; do \
		if test -d $$d; then \
			cp -a $$d $(debpkg)/$(instpath); \
		fi; \
	done
	if test -f README.md; then \
		mkdir -p $(debpkg)/$(docpath); \
		cp README.md $(debpkg)/$(docpath); \
	fi
	/usr/share/julia-pkg-dev/pkg-helper

clean::
	rm -f deps/deps.jl
